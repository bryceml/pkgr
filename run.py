#!/usr/bin/env python3
import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="pkgr")
    hub.pkgr.init.cli()


if __name__ == "__main__":
    start()
