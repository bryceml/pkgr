import pytest


@pytest.fixture(scope="function")
def hub(hub):
    """
    provides a full hub that is used as a reference for mock_hub
    """
    hub.pop.sub.add(dyne_name="pkgr")
    hub.pop.sub.add(dyne_name="config")
    hub.pop.sub.add(dyne_name="tiamat")
    yield hub
