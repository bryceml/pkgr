import pathlib
from unittest import mock


def test_build(mock_hub, hub):
    """
    Test correct paths and files are created
    when running hub.pkgr.rpm.build
    """
    # set test options
    mock_hub.OPT.pkgr = {"system": "rpm"}
    mock_hub.OPT.pkgr.sources = "sources"

    # create test files
    tar_file = mock_hub.pkgr.SDIR.join(
        "salt", "dist", f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}.tar.gz"
    )
    tar_file.write("", ensure=True)
    run_file = mock_hub.pkgr.CDIR.join("sources", "run.py")
    run_file.write("", ensure=True)
    for fp in ["SOURCES", "SPECS", "BUILD", "SRPMS"]:
        mock_hub.pkgr.BDIR.join(fp)

    proc = mock.MagicMock()
    proc.returncode = 0

    patch_proc = mock.patch("subprocess.run", return_value=proc)
    with patch_proc:
        mock_hub.pkgr.rpm.build = hub.pkgr.rpm.build
        mock_hub.pkgr.rpm.build()
    assert pathlib.Path(mock_hub.pkgr.CDIR, "artifacts").is_dir()
    assert (
        pathlib.Path(mock_hub.pkgr.BDIR, "SPECS", "pkg.spec").read_text()
        == "{ version }"
    )
    assert pathlib.Path(
        mock_hub.pkgr.BDIR,
        "SOURCES",
        mock_hub.pkgr.proj_name + "-" + mock_hub.pkgr.SALT_VERSION + ".tar.gz",
    ).is_file()
