import pathlib
import subprocess
from unittest import mock

from dict_tools import data


def test_build(mock_hub, hub):
    """
    Test correct paths and files are created
    when running hub.pkgr.tiamat.build
    """
    # set test options
    mock_hub.OPT.pkgr = {"system": "tiamat"}
    mock_hub.OPT.pkgr.sources = mock_hub.pkgr.CDIR.join("sources").ensure(dir=True)
    mock_hub.pkgr.SALT_VERSION = "4000"
    mock_hub.pkgr.PKG_VERSION = "99"
    mock_hub.OPT.tiamat = mock.MagicMock()

    # create test files
    tarball_name = f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}-{mock_hub.pkgr.PKG_VERSION}.tar.gz"
    tar_file = mock_hub.pkgr.SDIR.join("salt", "dist", tarball_name)
    tar_file.write("", ensure=True)
    run_file = mock_hub.pkgr.CDIR.join("sources", "run.py")
    run_file.write("", ensure=True)

    with mock.patch("subprocess.run") as patch_proc:
        patch_proc.returncode = 0
        mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
        mock_hub.pkgr.tiamat.build = hub.pkgr.tiamat.build
        mock_hub.pkgr.tiamat.build()
    dist = mock_hub.pkgr.SDIR.join("salt", "dist")
    patch_proc.assert_called_once_with(
        ["tar", "-czvf", tarball_name, "."],
        cwd=str(dist),
        shell=False,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        timeout=None,
        universal_newlines=True,
    )
