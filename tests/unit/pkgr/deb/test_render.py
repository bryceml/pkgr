import pathlib


def test_spec(mock_hub, hub):
    # set test options
    mock_hub.OPT.pkgr = {"system": "deb"}
    mock_hub.pkgr.SALT_VERSION = "4000"
    mock_hub.pkgr.PKG_VERSION = "99"
    mock_hub.OPT.pkgr.debian_dir = "debian"
    debian_dir = pathlib.Path(mock_hub.pkgr.CDIR) / mock_hub.OPT.pkgr.debian_dir
    debian_dir.mkdir(parents=True)
    changelog = debian_dir / "changelog"
    changelog.write_text("{{ version }}-{{pkg_version}}")
    # Hook into the real render method
    mock_hub.pkgr.deb.render = hub.pkgr.deb.render
    mock_hub.pkgr.deb.render()
    assert mock_hub.pkgr.CHANGELOG == "4000-99"
