from unittest import mock

import pytest


@pytest.mark.parametrize(
    "repo, name",
    [
        ("https://github.com/saltstack/salt.git", "salt"),
        ("git@gitlab.com:saltstack/open/salt-pkg.git", "salt-pkg"),
    ],
)
def test_proj_name(mock_hub, hub, repo, name):
    """
    Test gather() sets the correct proj_name
    """
    mock_hub.OPT.pkgr = {"git": repo, "ref": ""}
    mock_hub.pkgr.init.gather = hub.pkgr.init.gather
    mock_patch = mock.patch("subprocess.run", return_value=mock.MagicMock())
    with mock_patch:
        mock_hub.pkgr.init.gather()
    assert mock_hub.pkgr.proj_name == name
