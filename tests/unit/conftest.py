from unittest import mock

import pytest
from dict_tools import data


@pytest.fixture(scope="function")
def hub(hub):
    """
    provides a full hub that is used as a reference for mock_hub
    """
    # This way the virtual functions pass
    with mock.patch("shutil.which", return_value=True):
        hub.pop.sub.add(dyne_name="pkgr")

    hub.pop.sub.add(dyne_name="config")
    hub.pop.sub.add(dyne_name="tiamat")
    yield hub


@pytest.fixture(scope="function")
def mock_hub(mock_hub, tmpdir):
    mock_hub.OPT = data.NamespaceDict()
    mock_hub.pkgr.proj_name = "salt"
    mock_hub.pkgr.SALT_VERSION = mock_hub.pkgr.GIT_REF = "3000.1"
    mock_hub.pkgr.PKG_VERSION = 1
    mock_hub.pkgr.SDIR = tmpdir.mkdir("sdir")
    mock_hub.pkgr.CDIR = tmpdir.mkdir("cdir")
    mock_hub.pkgr.BDIR = tmpdir.mkdir("bdir")
    mock_hub.pkgr.SPEC = "{ version }"
    yield mock_hub
