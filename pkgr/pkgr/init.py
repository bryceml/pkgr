# Prepare primary sources as tarball
# Prep temp rpm build tree
# Run the spec file through jinja
# Save output into rpm build tree
# Copy sources into rpm build tree
# build rpm
import os
import pathlib
import tempfile


def __init__(hub):
    hub.pop.sub.add(dyne_name="tiamat")
    hub.pop.sub.load_subdirs(hub.pkgr)
    hub.pkgr.BDIR = tempfile.mkdtemp()
    hub.pkgr.SDIR = tempfile.mkdtemp()
    hub.pkgr.CDIR = os.getcwd()


def cli(hub):
    hub.pop.config.load(["pkgr", "tiamat"], cli="pkgr")
    hub.pkgr.init.gather()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.render", lambda: 0)()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.build")()


def gather(hub):
    hub.pkgr.ref.init.gather()
    git = hub.OPT.pkgr.git
    hub.pkgr.proj_name = git.rsplit("/", 1)[-1].split(".")[0]
    hub.tiamat.cmd.run(
        ["git", "clone", git],
        cwd=hub.pkgr.SDIR,
        fail_on_error=True,
    )

    if hub.pkgr.GIT_REF:
        tdir = os.path.basename(git)
        if tdir.endswith(".git"):
            tdir = tdir[:-4]
        hub.tiamat.cmd.run(
            ["git", "checkout", hub.pkgr.GIT_REF],
            cwd=pathlib.Path(hub.pkgr.SDIR) / tdir,
            fail_on_error=True,
        )
    hub.pkgr.ver.init.gather()
