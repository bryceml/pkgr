def gather(hub):
    """
    Gather the checkout ``ref`` based on the ``ver`` being used
    """
    # If we've been explicily passed a --pkg-version, use that
    pkg_version = None
    if hub.OPT.pkgr.pkg_version:
        pkg_version = hub.OPT.pkgr.pkg_version

    if not hub.OPT.pkgr.ref:
        hub.pkgr.PKG_VERSION = pkg_version or "1"
        hub.pkgr.GIT_REF = None
        return

    ver = hub.OPT.pkgr.ver
    git_ref = hub.OPT.pkgr.ref
    if ver in hub.pkgr.ref:
        git_ref, _pkg_version = getattr(hub, f"pkgr.ref.{ver}.gather")()
        if pkg_version is None:
            # We haven't been explicitly passed --pkg-version
            pkg_version = _pkg_version
    hub.pkgr.GIT_REF = git_ref or hub.OPT.pkgr.ref
    hub.pkgr.PKG_VERSION = pkg_version or "1"
