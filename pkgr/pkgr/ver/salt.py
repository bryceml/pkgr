import os
import pathlib


def gather(hub):
    """
    Gather the version number from salt
    """
    pybin = hub.OPT.pkgr.python
    cdir = pathlib.Path(hub.pkgr.SDIR, "salt")
    if hub.OPT.pkgr.tiamat_build:
        hub.tiamat.cmd.run(
            [pybin, "setup.py", "sdist"],
            cwd=cdir,
            fail_on_error=True,
        )
    else:
        git_describe = hub.tiamat.cmd.run(
            [
                "git",
                "describe",
                "--tags",
                "--long",
                "--match",
                "v[0-9]*",
            ],
            cwd=cdir,
            fail_on_error=True,
        ).stdout.split("-")
        # if we're not building a tag, tag something like v3002nb301
        if git_describe[1] != "0":
            git_tag = git_describe[0] + "nb" + git_describe[1]
            hub.tiamat.cmd.run(["git", "tag", git_tag], cwd=cdir, fail_on_error=True)

        hub.tiamat.cmd.run(
            ["env", "--unset=TIAMAT_BUILD", pybin, "setup.py", "sdist"],
            cwd=cdir,
            fail_on_error=True,
        )

    version = None
    for fn in cdir.joinpath("dist").iterdir():
        fn = str(fn)
        version = fn[fn.index("-") + 1 : fn[: fn.rindex(".")].rindex(".")]
    return version
