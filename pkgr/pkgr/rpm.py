import datetime
import os
import shutil

import jinja2


def __virtual__(hub):
    required_bins = {"tar", "rpmbuild"}
    for rbin in list(required_bins):
        if shutil.which(rbin) is not None:
            required_bins.remove(rbin)
    if required_bins:
        return (
            False,
            f"The following binaries were not found on the system: {', '.join(required_bins)}",
        )
    return True


def build(hub):
    """
    Build the package!
    """
    # Prep the build tree
    # Copy the sources in
    # place the spec file down with the changes from the jinja template
    # rpmbuild -ba
    # copy results of build to artifacts folder
    proj = os.path.join(
        hub.pkgr.SDIR,
        hub.pkgr.proj_name,
        "dist",
        f"{hub.pkgr.proj_name}-{hub.pkgr.SALT_VERSION}.tar.gz",
    )

    sources = os.path.join(hub.pkgr.BDIR, "SOURCES")
    spec = os.path.join(hub.pkgr.BDIR, "SPECS", f"{hub.pkgr.proj_name}.spec")
    rpms = os.path.join(hub.pkgr.BDIR, "RPMS")
    srpms = os.path.join(hub.pkgr.BDIR, "SRPMS")

    hub.log.debug(f"[pkgr] Copying sources from {hub.OPT.pkgr.sources} to {sources}")
    shutil.copytree(hub.OPT.pkgr.sources, sources)
    hub.log.debug(f"[pkgr] Copying {proj} to {sources}")
    shutil.copy2(proj, sources)
    hub.log.debug(f"[pkgr] Writing spec to {spec}")
    os.makedirs(os.path.join(hub.pkgr.BDIR, "SPECS"))
    with open(spec, "w+") as wfh:
        wfh.write(hub.pkgr.SPEC)

    if hub.OPT.pkgr.tiamat_build:
        hub.tiamat.cmd.run(
            ["rpmbuild", "--define", f"_topdir {hub.pkgr.BDIR}", "-ba", spec],
            fail_on_error=True,
        )
    else:
        hub.tiamat.cmd.run(
            [
                "env",
                "--unset=TIAMAT_BUILD",
                "rpmbuild",
                "--define",
                f"_topdir {hub.pkgr.BDIR}",
                "-ba",
                spec,
            ],
            fail_on_error=True,
        )

    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts")
    if not os.path.isdir(artifacts):
        hub.log.debug(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    for root, dirs, files in os.walk(rpms):
        for fn in files:
            if fn.endswith("rpm"):
                hub.log.debug(f"[pkgr] Copying {fn} to {artifacts}")
                shutil.copy2(os.path.join(root, fn), artifacts)
    for root, dirs, files in os.walk(srpms):
        for fn in files:
            if fn.endswith("rpm"):
                hub.log.debug(f"[pkgr] Copying {fn} to {artifacts}")
                shutil.copy2(os.path.join(root, fn), artifacts)


def render(hub):
    """
    Render the spec file
    """
    opts = dict(hub.OPT.pkgr)
    salt_version = hub.pkgr.SALT_VERSION.replace("rc", "~rc").replace("-", "_")
    opts["version"] = salt_version
    opts["pkg_version"] = hub.pkgr.PKG_VERSION
    utcnow = datetime.datetime.now(tz=datetime.timezone.utc)
    opts["changelog_date"] = utcnow.strftime("%a %b %d %Y")
    with open(os.path.join(hub.pkgr.CDIR, hub.OPT.pkgr.spec)) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.SPEC = template.render(**opts)
